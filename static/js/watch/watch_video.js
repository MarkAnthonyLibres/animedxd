(function (jq) {

    function isVideo(url) {
        return new Promise(function (res, rej) {

            var video = document.createElement('video');
            video.preload = 'metadata';
            video.onloadedmetadata = function (evt) {
                res(!!(video.videoHeight && video.videoWidth));
                video.src = "about:blank";
            };
            video.onerror = function () {
                rej();
            }

            console.log(url);

            video.src = url;
        });
    }

    const iframe = jq(".live_video");
    iframe.ready(function () {

        const of_src = iframe.attr("of_src");

        const new_iframe = iframe.clone();
        new_iframe.css("display", "none");
        new_iframe.attr("src", of_src);
        iframe.after(new_iframe);
        new_iframe.ready(setTimeout(function(){
            iframe.hide();
            new_iframe.css("display", "block");
        },2000))

    });

})(jQuery)

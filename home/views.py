import sys;

from django.shortcuts import render, HttpResponse
from django.contrib.auth.models import User, auth
from django.contrib import messages
from django.contrib.auth import logout
from modules import helpers
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required, user_passes_test;
from datetime import datetime;
from django.db.models import Q, Value, Count;
from django.core.paginator import Paginator;
import pdb;
import requests
import base64
from app.scrapper.ongoing_series import OngoingSeries
from app.scrapper.watch_video import WatchVideo
from app.scrapper.search import Search




def home(request):

    import datetime
    import dateutil.relativedelta
    from modules.anime_query.query import get_latest_episodes, get_popular_shows;
    import random


    of_data = get_latest_episodes();
    of_data["all"] = of_data["all"][0:15]
    popular_shows = get_popular_shows();

    page = request.GET['page'] if "page" in request.GET else 1
    recent_added = OngoingSeries(page=page)

    return render(request, "index.html", {
        "ongoing" : recent_added,
        "latest" : of_data["all"],
        "banner" : of_data["with_banner"][0:15],
        "popular_shows" : popular_shows
    });

    pass;

def watch(request, identifier):
    value_bytes = str(identifier).encode('ascii');
    link_path = base64.b64decode(value_bytes).decode("utf-8");

    watch_details = WatchVideo(link=link_path)

    return render(request, 'anime_watch.html', {
        'watch_details' : watch_details
    })

    pass

def of_search(request):

    if not "keyword" in request.GET:
        return redirect("/");

    search_details = Search(keyword=request.GET.get("keyword"))

    return render(request, "anime_search.html", {
        "search_details" : search_details
    })


    pass

def anime_details(request, id):

    from modules.anime_query.query import get_anime_details;
    from anime_downloader.sites import get_anime_class
    from datetime import datetime
    from modules.anime_server import  get_site_base
    import pytz

    details = get_anime_details(id)
    details['next_date_airing'] = None;

    if details['nextAiringEpisode'] != None :

        details['next_date_airing'] = details['nextAiringEpisode']['airingAt'];
        local_tz = pytz.timezone("Asia/Singapore")
        utc_dt = datetime.utcfromtimestamp(details['next_date_airing']).replace(tzinfo=pytz.utc)
        local_dt = local_tz.normalize(utc_dt.astimezone(local_tz))

        details['next_date_airing'] = local_dt;

        pass;

    site_base = get_site_base(request);

    anime_twerk = site_base['anime_class'];
    search = anime_twerk.search(details["title"]["romaji"]);

    if len(search) <= 0 and details["title"]['english']:
        search = anime_twerk.search(details["title"]["english"]);

    if len(search) <= 0 and details["title"]['userPreferred']:
        search = anime_twerk.search(details["title"]["english"]);

    if len(search) <= 0 and len(details["synonyms"]) > 0:
        of_search = details["synonyms"][0];
        search = anime_twerk.search(of_search);
        pass;


    return render(request, "anime-details.html", {
        "details" : details,
        "watch_list" : search,
        "format_index_base" : site_base['index']
    });

    pass;


def anime_watch(request,id, value, episode, format_base_index):

    import base64
    from modules.anime_query.query import get_basic_details;
    from modules.anime_server import SITES;

    site_base = SITES[str(format_base_index)];
    decode_url = site_base['decode_url'](value);

    return render(request, "anime-watching.html", {
        "episode" : episode,
        "title" : decode_url.title,
        "site_value" : value,
        "site_base_index" : format_base_index,
        "by_episode" : range(len(decode_url) + 1),
        "id" : id
    });


    pass;

def on_load_video(request,value,episode, site_base_index):

    import base64
    from anime_downloader.sites.anime8 import Anime8 as twerk;
    from modules.anime_server import SITES;

    site_base = SITES[str(site_base_index)];
    decode_url = site_base['decode_url'](value);
    print(dir(decode_url[episode - 1].source()))
    print(decode_url[episode - 1].source().url)
    return HttpResponse(decode_url[episode - 1].source().stream_url);

    pass;


def search(request):

    from modules.anime_query.query import search;

    if not "search" in request.GET:
        return redirect("/");

    search_value = request.GET['search'];
    result = search(search_value);

    return render(request, "search.html", {
        "details" : result,
        "search_on" : search_value
    });

    pass;

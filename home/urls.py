from django.urls import path;
from django.contrib import admin
from . import views

urlpatterns = [
    path("", views.home, name="home"),
    path("home", views.home, name="home"),
    # path("details/<int:id>", views.anime_details, name="details"),
    # path("watch/<int:id>/<int:episode>/<int:format_base_index>/<str:value>", views.anime_watch, name="watch"),
    # path("onload/<int:episode>/<int:site_base_index>/<str:value>", views.on_load_video, name="load_video"),
    # path("search", views.search, name="search"),
    path("watch/<str:identifier>",views.watch,name="watch"),
    path("search",views.of_search,name="search")
];

from django import template
from modules.helpers import _String
register = template.Library()
from urllib.parse import urlparse
import base64
from modules.anime_server import  SITES


@register.filter
def anime_url(site_base, format_base_index):
   return SITES[str(format_base_index)]['encode_url'](site_base);


@register.filter
def episode_leading_zero(str_episode):
   return str(str_episode).zfill(2);


@register.filter
def trim(value):
    return ""
    # return value.strip()

@register.filter
def toBase64(value):
    message_bytes = str(value).encode('ascii')
    return base64.b64encode(message_bytes).decode("utf-8")
    pass

(function (jq) {

    function isVideo(url) {
        return new Promise(function (res, rej) {
            var video = document.createElement('video');
            video.preload = 'metadata';
            video.onloadedmetadata = function (evt) {
                res(!!(video.videoHeight && video.videoWidth));
                video.src = null;
            };
            video.onerror = function () {
                rej();
            }

            video.src = url;
        });
    }

    const iframe = jq(".live_video");
    iframe.ready(function () {

        const value = iframe.attr("token");
        const episode = iframe.attr("episode");
        const site_base = iframe.attr("site_base");
        const url = "/onload/" + episode + "/" + site_base + "/" + value;

        const send = jq.ajax({
            url: url,
            method: "GET",
        });

        send.done(function (value) {

            const is_video = isVideo(value);

            is_video.then(function (result) {

                const video = jQuery("<video>");
                video.attr("controls","");
                video.addClass("live_video");
                video.css("height","auto");
                video.hide();
                iframe.after(video);
                video.attr("src", value);
                video.on("canplay", function () {
                    iframe.hide();
                    video.show();
                })
            });

            is_video.catch(function () {

                const new_iframe = iframe.clone();
                new_iframe.css("display", "none");
                new_iframe.attr("src", value);
                iframe.after(new_iframe);
                new_iframe.ready(function () {
                    iframe.hide();
                    new_iframe.css("display", "block");
                })

            });

        });

        send.catch(function () {
            alert('Error loading video');
        })

    });

})(jQuery)
from anime_downloader.sites import get_anime_class
from urllib.parse import urlparse
from anime_downloader.sites import anime8, shiro, fastani,subsplease, animeout
import base64

def anime8_url_encode(value):

    of_url = urlparse(value)
    of_path = of_url.path
    chunk = of_path.split("/")
    chunk = [x for x in chunk if x]
    chunk = "/".join(chunk)

    message_bytes = str(of_path).encode('ascii')
    return base64.b64encode(message_bytes).decode("utf-8")

    pass;


def anime8_url_decode(value):

    value_bytes = str(value).encode('ascii');
    of_value = base64.b64decode(value_bytes).decode("utf-8");
    print(of_value)
    url_form = "/".join(["https://subsplease.org", of_value]);

    return subsplease.SubsPlease(url_form);

    pass;


def shiro_url_decode(value):

    value_bytes = str(value).encode('ascii');
    of_value = base64.b64decode(value_bytes).decode("utf-8");
    url_form = "/".join(["https://shiro.is/anime", of_value]);

    return shiro.Shiro(url_form);

    pass;

def shiro_url_encode(value):
    return anime8_url_encode(value);
    pass;


def fastani_url_decode(value):

    value = str(value).replace("&","/");
    url_form = "/".join(["https://fastani.net", value]);
    print("HELLO >> ",url_form, value);
    return fastani.FastAni(url_form);

    pass;

def fastani_url_encode(value):

    of_url = urlparse(value)
    of_path = of_url.path;
    chunk = of_path.split("/");
    url_form = "&".join(chunk);

    return url_form;

    pass;

SITES = {
    "0" : {
        "anime_class" : get_anime_class("animeout"),
        "index" : 0,
        "encode_url" : anime8_url_encode,
        "decode_url" : anime8_url_decode,
        "base" : animeout
    },
    "1" : {
        "anime_class" : get_anime_class("shiro"),
        "index" : 1,
        "encode_url" : shiro_url_encode,
        "decode_url" : shiro_url_decode,
        "base" : shiro
    }
};




def get_site_base(request):

    if not request.user_agent.is_mobile:

        return SITES["0"];

    return SITES["0"]


    pass;

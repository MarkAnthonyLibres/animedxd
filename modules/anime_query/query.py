import requests;
import json
from anime_downloader.sites import get_anime_class


def get_latest_episodes():
    query = '''
    query ($id: Int, $page: Int, $perPage: Int, $airing_timestamp : Int) {
        Page (page: $page, perPage: $perPage) {
            airingSchedules(id: $id, notYetAired : false, airingAt_greater : $airing_timestamp, sort: TIME_DESC ) {
                timeUntilAiring
                episode
                airingAt
                media {
                    id
                    description(asHtml : false)
                    nextAiringEpisode {
                        episode
                    }
                    title {
                        romaji
                        english
                        userPreferred
                    }
                    episodes
                    duration
                    countryOfOrigin
                    bannerImage
                    genres
                    popularity
                    coverImage {
                        extraLarge
                        large
                        medium
                    }
                    meanScore
                    averageScore
                    tags {
                        category
                    }

                }
            }
        }

    }
    '''

    variables = {
        'page': 1,
        'perPage': 100,
        'airing_timestamp': 1612139028
    }

    url = 'https://graphql.anilist.co'

    response = requests.post(url, json={'query': query, 'variables': variables});

    as_json = response.json();
    data = as_json["data"]["Page"]["airingSchedules"];
    of_data = [];
    with_banner = [];

    for per in data:

        if per['media']['countryOfOrigin'] != "JP":
            continue;
            pass;

        of_data.append(per);

        if not per['media']['bannerImage']:
            continue;
            pass;

        with_banner.append(per);

        pass;

    return {"all": of_data, "with_banner": with_banner}

    pass;


def get_anime_details(id):
    query = '''
           query ($id: Int) {
               Media (id: $id) {
                   id
                   title {
                        romaji
                        english
                        native
                        userPreferred
                   }
                   type
                   format
                   status
                   description
                   startDate {
                        year
                        month
                        day
                   }
                   endDate {
                        year
                        month
                        day
                   }
                   season
                   seasonYear
                   seasonInt
                   episodes
                   duration
                   countryOfOrigin
                   hashtag
                   trailer {
                        id
                        site
                        thumbnail
                   }
                   updatedAt
                   coverImage {
                        extraLarge
                        large
                        medium
                   }
                   bannerImage
                   genres
                   synonyms
                   averageScore
                   meanScore
                   popularity
                   favourites
                   trending
                   isAdult
                   reviews {
                        nodes {
                            userId
                            summary
                            body
                            rating
                            ratingAmount
                            userRating
                            score
                            createdAt
                            user {
                                name
                                avatar {
                                    large
                                }
                            }
                        }
                   }
                   nextAiringEpisode {
                        episode
                        airingAt
                        timeUntilAiring
                   }
                   airingSchedule {
                        nodes {
                            airingAt
                            timeUntilAiring
                        }
                   }
               }

           }
           '''

    variables = {
        'id': id,
    }

    url = 'https://graphql.anilist.co'

    response = requests.post(url, json={'query': query, 'variables': variables});

    as_json = response.json();

    return as_json["data"]["Media"];

    pass;


def get_basic_details(id):
    query = '''
               query ($id: Int) {
                   Media (id: $id) {
                       id
                       title {
                            romaji
                            english
                            native
                            userPreferred
                       }
                   }

               }
               '''

    variables = {
        'id': id,
    }

    url = 'https://graphql.anilist.co'

    response = requests.post(url, json={'query': query, 'variables': variables});

    as_json = response.json();

    return as_json["data"]["Media"];

    pass;


def search(value):
    query = '''
        query ($page: Int, $perPage: Int, $search : String) {
            
            Page (page: $page, perPage: $perPage) {
                pageInfo {
                    total
                    perPage
                    currentPage
                    lastPage
                    hasNextPage
                }
                media(search: $search, isAdult: false, sort: POPULARITY_DESC) {
                    id
                    description(asHtml : false)
                    nextAiringEpisode {
                        episode
                    }
                    title {
                        romaji
                        english
                    }
                    episodes
                    duration
                    countryOfOrigin
                    bannerImage
                    genres
                    popularity
                    coverImage {
                        extraLarge
                        large
                        medium
                    }
                    meanScore
                    averageScore
                    tags {
                        category
                    }
                }
            }

        }
        ''';

    variables = {
        'page': 1,
        'perPage': 18,
        'search': value
    }

    url = 'https://graphql.anilist.co'

    response = requests.post(url, json={'query': query, 'variables': variables});

    as_json = response.json();
    to_data = as_json['data'];
    media = to_data["Page"]["media"];
    to_data["Page"]["media"] = [];

    for per in media:

        if per['countryOfOrigin'] != "JP":
            continue;
            pass;

        to_data["Page"]["media"].append(per);

        pass;

    return to_data;

    pass;


def get_popular_shows():
    from datetime import date

    query = '''
                query ($page: Int, $perPage: Int, $date : FuzzyDateInt) {

                    Page (page: $page, perPage: $perPage) {
                        pageInfo {
                            total
                            perPage
                            currentPage
                            lastPage
                            hasNextPage
                        }
                        media(isAdult: false, sort: POPULARITY_DESC, startDate_greater: $date   ) {
                            id
                            description(asHtml : false)
                            nextAiringEpisode {
                                episode
                            }
                            title {
                                romaji
                            }
                            episodes
                            duration
                            countryOfOrigin
                            bannerImage
                            genres
                            popularity
                            coverImage {
                                extraLarge
                                large
                                medium
                            }
                            meanScore
                            averageScore
                            tags {
                                category
                            }
                        }
                    }

                }
                ''';

    today = date.today();

    variables = {
        'page': 1,
        'perPage': 5,
        'date': int(today.strftime("%Y0000"))
    }

    url = 'https://graphql.anilist.co'

    response = requests.post(url, json={'query': query, 'variables': variables});

    as_json = response.json();

    to_data = as_json['data'];
    media = to_data["Page"]["media"];
    to_data["Page"]["media"] = [];

    for per in media:

        if per['countryOfOrigin'] != "JP":
            continue;
            pass;

        to_data["Page"]["media"].append(per);

        pass;

    return to_data;

    pass;

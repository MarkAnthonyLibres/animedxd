

class LinkAttr():
    link = None
    img_link = None
    img_alt = None
    name = None
    meta = None

class Links():

    def get_list(self, soup=None):
        arr = list()
        soup = soup or self.soup
        listing_items = soup.find_all("ul", class_="listing items")
        all_list = soup.find_all("li", class_="video-block")

        for per in all_list:
            link = per.find("a")
            img = per.find("div",class_="picture").find("img")
            name = per.find("div",class_="name").get_text()
            meta = per.find("div",class_="meta")\
                .find("span", class_="date").get_text()

            attr = LinkAttr()
            attr.link = link['href']
            attr.img_link = img['src']
            attr.img_alt = img['alt']
            attr.name = name
            attr.meta = meta
            arr.append(attr)
            pass
        return arr
        pass

    pass

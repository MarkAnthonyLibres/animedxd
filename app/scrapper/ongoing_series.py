import requests
from bs4 import BeautifulSoup
from datetime import datetime
from urllib.parse import urlparse, parse_qs
from app.scrapper.link_list import Links
import cloudscraper

class PaginationStatus():
    link = None
    is_active = False
    is_previous = False
    is_next = False
    name = None
    pass

class OngoingSeries(Links):
    def __init__(self, page=1):
        scraper = cloudscraper.create_scraper()
        self.URL = 'https://gogo-stream.com/ongoing-series?page=' + str(page)
        content = scraper.get(self.URL).text
        # page = requests.get(self.URL)
        self.soup = BeautifulSoup(content, 'html.parser')
        pass

    def get_pagination(self):
        listing_items = self.soup.find("ul", class_="pagination")
        print(self.soup)
        items = listing_items.find_all("li")
        arr = list()

        for per in items:
            link = per.find("a")
            classes = per['class'] if "class" in per.attrs else []
            pagination = PaginationStatus()
            pagination.link = link['href']
            pagination.is_previous = "previous" in classes
            pagination.is_active = "active" in classes
            pagination.is_next = "next" in classes
            pagination.name = link.get_text()
            arr.append(pagination)
            pass

        return arr

        pass

    pass

init = OngoingSeries()
init.get_pagination();
# print(init.arr)



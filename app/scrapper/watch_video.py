import requests
from bs4 import BeautifulSoup
from app.scrapper.link_list import Links
import re


class WatchVideo(Links):
    arr = list()
    title = None
    name = None
    episode = None
    details = None

    TITLE_SUB = [
        "English Subbed"
    ]

    def __init__(self, link):
        self.URL = 'https://gogo-stream.com/' + str(link)
        page = requests.get(self.URL)
        self.soup = BeautifulSoup(page.content, 'html.parser')

        self.title = self.soup.find("div", class_="video-info-left")\
            .find("h1")\
            .get_text()\
            .strip()

        self.name = self.soup\
            .find("div", class_="video-details")\
            .find("span")\
            .get_text()\
            .strip()

        self.details = self.soup\
            .find("div",class_="post-entry")\
            .find("div",class_="content-more-js")\
            .get_text()

        title_sub = self.title

        for per in self.TITLE_SUB:
            title_sub = title_sub.replace(per,"")
            pass

        self.episode = title_sub.replace(self.name,"").strip()



    def video(self):

        watch_play_container = self.soup.find("div", class_="watch_play")
        iframe_link = watch_play_container.find("iframe");

        return iframe_link['src']

        pass


    def get_episodes(self):
        soup = self.soup.find("div", class_="video-info-left")
        list = self.get_list(soup=soup)
        for per in list:
            per.name = per.name.replace(self.name,"").strip()
            pass

        return list

        pass



    pass


import requests
from bs4 import BeautifulSoup
from app.scrapper.link_list import Links
import re


class Search(Links):
    keyword = None

    def __init__(self, keyword):
        self.keyword = keyword
        self.URL = 'https://gogo-stream.com/search.html?keyword=' + str(keyword)
        page = requests.get(self.URL)
        self.soup = BeautifulSoup(page.content, 'html.parser')


    pass


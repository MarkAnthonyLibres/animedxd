import requests
from bs4 import BeautifulSoup
from datetime import datetime
from urllib.parse import urlparse, parse_qs

class RecentAttr():
    link = None
    img_link = None
    img_alt = None
    name = None
    meta = None

class RecentAdded():
    arr = list()
    def __init__(self, page=1):
        self.URL = 'https://gogo-stream.com/?page=' + str(page)
        page = requests.get(self.URL)
        soup = BeautifulSoup(page.content, 'html.parser')
        listing_items = soup.find_all("ul", class_="listing items")
        all_list = soup.find_all("li", class_="video-block")

        for per in all_list:
            link = per.find("a")
            img = per.find("div",class_="picture").find("img")
            name = per.find("div",class_="name").get_text()
            meta = per.find("div",class_="meta")\
                .find("span", class_="date").get_text()

            attr = RecentAttr()
            attr.link = link['href']
            attr.img_link = img['src']
            attr.img_alt = img['alt']
            attr.name = name
            attr.meta = meta
            self.arr.append(attr)
            pass

    pass


